<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Exception;

use RuntimeException;

final class PasswordExposure extends RuntimeException
{
	public static function viaWakeup(): self
	{
		return new self('This object can not be deserialized');
	}

	public static function viaSleep(): self
	{
		return new self('This object can not be serialized');
	}

	public static function viaClone(): self
	{
		return new self('This object can not be cloned');
	}
}
