<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Filters;

final class FilterListFactory
{
	public static function create(): FilterList
	{
		return FilterList::fromFilters(
			new NormalizeUnicodeCharacters(),
			new RemoveMultipleSpaces(),
		);
	}
}
