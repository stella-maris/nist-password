<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Filters;

use Normalizer;
use StellaMaris\Password\Password;

/**
 * If Unicode characters are accepted in memorized secrets, the verifier SHOULD apply the Normalization Process for
 * Stabilized Strings using either the NFKC or NFKD normalization defined in Section 12.1 of Unicode Standard Annex
 * 15 [UAX 15]. This process is applied before hashing the byte string representing the memorized secret
 */
final class NormalizeUnicodeCharacters implements Filter
{
	public function filter(Password $password): Password
	{
		return Password::fromString(Normalizer::normalize(
			$password->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave(),
			Normalizer::NFKC
		));
	}
}
