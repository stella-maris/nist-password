<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Filters;

use StellaMaris\Password\Password;

interface Filter
{
	public function filter(Password $password): Password;
}
