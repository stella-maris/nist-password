<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Filters;

use StellaMaris\Password\Password;
use function preg_replace;

/**
 * To make allowances for likely mistyping, verifiers MAY replace multiple consecutive space characters with a single
 * space character prior to verification, provided that the result is at least 8 characters in length.
 */
final class RemoveMultipleSpaces implements Filter
{
	public function filter(Password $password): Password
	{
		return Password::fromString(preg_replace(
			'/[ ]{2,}/',
			' ',
			$password->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave()
		));
	}
}
