<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Filters;

use Iterator;
use StellaMaris\Password\IteratorImplementation;

final class FilterList implements Iterator
{
	use IteratorImplementation;

	/** @var Filter[] */
	private array $filter = [];

	private function __construct(Filter ...$filter)
	{
		$this->filter = $filter;
	}

	public static function empty(): self
	{
		return new self();
	}

	public static function fromArray(array $filters): self
	{
		$list = self::empty();
		foreach ($filters as $filter) {
			if (! $filter instanceof Filter) {
				continue;
			}

			$list = $list->withFilter($filter);
		}

		return $list;
	}

	public static function fromFilters(Filter ...$filters): self
	{
		return new self(...$filters);
	}

	public function withFilter(Filter $filter): self
	{
		return new self($filter, ...$this->filter);
	}

	private function &getList(): array
	{
		return $this->filter;
	}
}
