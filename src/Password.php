<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licensed under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password;

use SodiumException;
use StellaMaris\Password\Exception\PasswordExposure;
use function define;
use function password_hash;
use function password_needs_rehash;
use function password_verify;
use function random_bytes;
use function sodium_crypto_secretbox;
use function sodium_crypto_secretbox_keygen;
use function sodium_crypto_secretbox_open;
use const PASSWORD_DEFAULT;
use const SODIUM_CRYPTO_SECRETBOX_NONCEBYTES;
use const STELLA_MARIS_PASSWORD_PASSWORD_KEY;
use const STELLA_MARIS_PASSWORD_PASSWORD_NONCE;

final class Password
{
	private ?string $hash = null;

	private string $password;

	/**
	 * @throws SodiumException
	 */
	private function __construct(string $password)
	{
		$this->hash = null;
		$this->password = sodium_crypto_secretbox(
			$password,
			(string) STELLA_MARIS_PASSWORD_PASSWORD_NONCE,
			(string) STELLA_MARIS_PASSWORD_PASSWORD_KEY
		);
	}

	public static function fromString(string $password): self
	{
		return new self($password);
	}

	public function matchesHash(string $hash): bool
	{
		$this->hash = $hash;

		return password_verify(
			$this->getPasswordInPlainText(),
			$this->hash
		);
	}

	public function needsRehash(string $algorithm = PASSWORD_DEFAULT, array $options = []): bool
	{
		if (null === $this->hash) {
			return true;
		}

		return password_needs_rehash($this->hash, $algorithm, $options);
	}

	public function hash(string $algorithm = PASSWORD_DEFAULT, array $options = []): string
	{
		return password_hash($this->getPasswordInPlainText(), $algorithm, $options);
	}

	public function yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave(): string
	{
//		trigger_error(
//			'Password was leaked in clear text using the ' .
//			'"Password::yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave"-function!!',
//			E_USER_WARNING
//		);

		return $this->getPasswordInPlainText();
	}

	private function getPasswordInPlainText(): string
	{
		return sodium_crypto_secretbox_open(
			$this->password,
			(string) STELLA_MARIS_PASSWORD_PASSWORD_NONCE,
			(string) STELLA_MARIS_PASSWORD_PASSWORD_KEY
		);
	}

	public function __toString()
	{
		return '********';
	}

	public function __debugInfo()
	{
		return [
			'password' => '********',
		];
	}

	/**
	 * @throws PasswordExposure
	 */
	public function __wakeup(): void
	{
		throw PasswordExposure::viaWakeup();
	}

	/**
	 * @throws PasswordExposure
	 */
	public function __sleep(): array
	{
		throw PasswordExposure::viaSleep();
	}

	/**
	 * @throws PasswordExposure
	 */
	public function __clone()
	{
		throw PasswordExposure::viaClone();
	}
}

define('STELLA_MARIS_PASSWORD_PASSWORD_NONCE', random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES));
define('STELLA_MARIS_PASSWORD_PASSWORD_KEY', sodium_crypto_secretbox_keygen());
