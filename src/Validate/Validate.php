<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licensed under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate;

use StellaMaris\Password\Filters\Filter;
use StellaMaris\Password\Filters\FilterList;
use StellaMaris\Password\Password;

final class Validate
{
	public function __construct(
		private ValidatorList $validators,
		private FilterList $filters,
	) {
    }

	public function validate(Password $password): Feedback
	{
		/** @var Filter $filter */
		foreach ($this->filters as $filter) {
			$password = $filter->filter($password);
		}

		$feedback = new Feedback($password);

		/** @var Validator $validator */
		foreach ($this->validators as $validator) {
			if ($validator->validate($password)) {
				continue;
			}
			$feedback->addProblem($validator->getProblem());
		}

		return $feedback;
	}
}
