<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate;

final class Problem
{
	public function __construct(private string $message)
    {
    }

	public function getMessage(): string
	{
		return $this->message;
	}
}
