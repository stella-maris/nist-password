<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate;

use Dragonbe\Hibp\HibpFactory;
use StellaMaris\Password\Validate\Validators\MinimumEightCharacters;
use StellaMaris\Password\Validate\Validators\NoRepetitiveCharacters;
use StellaMaris\Password\Validate\Validators\NoSequentialCharacters;
use StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach;

final class ValidatorListFactory
{
	public static function create(): ValidatorList
	{
		return ValidatorList::fromValidators(
			new MinimumEightCharacters(),
			new PasswordHasNotBeenExposedInBreach(HibpFactory::create()),
			new NoSequentialCharacters(),
			new NoRepetitiveCharacters(),
		);
	}
}
