<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate\Validators;

use IntlChar;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validator;
use function mb_str_split;

/**
 * When processing requests to establish and change memorized secrets, verifiers SHALL compare the prospective secrets
 * against a list that contains values known to be commonly-used, expected, or compromised. For example, the list MAY
 * include, but is not limited to:
 *
 * * Passwords obtained from previous breach corpuses.
 * * Dictionary words.
 * * Repetitive or sequential characters (e.g. ‘aaaaaa’, ‘1234abcd’).
 * * Context-specific words, such as the name of the service, the username, and derivatives thereof.
 *
 * If the chosen secret is found in the list, the CSP or verifier SHALL advise the subscriber that they need to select
 * a different secret, SHALL provide the reason for rejection, and SHALL require the subscriber to choose a
 * different value. */
final class NoSequentialCharacters implements Validator
{
	private const UP = 'up';

	private const DOWN = 'down';

	public function validate(Password $password): bool
	{
		$prev = null;
		$direction = null;
		$counter = 0;
		foreach (mb_str_split($password->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave()) as $char) {
			$char = IntlChar::ord(IntlChar::tolower($char));
			if ($prev === null) {
				$prev =$char;
				continue;
			}

			$diff = $char - $prev;
			$prev = $char;

			switch ($diff) {
				case -1:
					$dir = self::UP;
					break;
				case 1:
					$dir = self::DOWN;
					break;
				default:
					$counter = 0;
					$direction = null;
					continue 2;
			}

			if ($dir !== $direction) {
				$direction = $dir;
				$counter = 0;
			}

			++$counter;

			if ($counter >= 3) {
				return false;
			}
		}
		return true;
	}

	public function getProblem(): Problem
	{
		return new Problem('The password contains sequential characters');
	}
}
