<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate\Validators;

use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validator;
use function mb_strlen;

/**
 * Verifiers SHALL require subscriber-chosen memorized secrets to be at least 8 characters in length.
 */
final class MinimumEightCharacters implements Validator
{
	public function validate(Password $password): bool
	{
		return 8 <= mb_strlen($password->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave());
	}

	public function getProblem(): Problem
	{
		return new Problem('The password has less than 8 characters');
	}
}
