<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate\Validators;

use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validator;
use function preg_match;

/**
 * When processing requests to establish and change memorized secrets, verifiers SHALL compare the prospective secrets
 * against a list that contains values known to be commonly-used, expected, or compromised. For example, the list MAY
 * include, but is not limited to:
 *
 * * Passwords obtained from previous breach corpuses.
 * * Dictionary words.
 * * Repetitive or sequential characters (e.g. ‘aaaaaa’, ‘1234abcd’).
 * * Context-specific words, such as the name of the service, the username, and derivatives thereof.
 *
 * If the chosen secret is found in the list, the CSP or verifier SHALL advise the subscriber that they need to select
 * a different secret, SHALL provide the reason for rejection, and SHALL require the subscriber to choose a
 * different value. */
final class NoRepetitiveCharacters implements Validator
{
	public function validate(Password $password): bool
	{
		return ! preg_match('/(.+){3,}/', $password->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave());
	}

	public function getProblem(): Problem
	{
		return new Problem('The password contains repetitive characters');
	}
}
