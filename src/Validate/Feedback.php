<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate;

use Iterator;
use StellaMaris\Password\IteratorImplementation;
use StellaMaris\Password\Password;

final class Feedback implements Iterator
{
	use IteratorImplementation;

	private array $problems = [];

	public function __construct(private Password $password)
    {
    }

	public function addProblem(Problem $problem): void
	{
		$this->problems[] = $problem;
	}

	public function isValid(): bool
	{
		return $this->problems === [];
	}

	public function getPassword(): Password
	{
		return $this->password;
	}

	private function &getList(): array
	{
		return $this->problems;
	}
}
