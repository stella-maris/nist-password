<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password\Validate;

use Iterator;
use StellaMaris\Password\IteratorImplementation;

final class ValidatorList implements Iterator
{
	use IteratorImplementation;

	/** @var Validator[] */
	private array $validators = [];

	private function __construct(Validator ...$validators)
	{
		$this->validators = $validators;
	}

	public static function empty(): self
	{
		return new self();
	}

	public static function fromArray(array $validators): self
	{
		$list = self::empty();
		foreach ($validators as $validator) {
			if (! $validator instanceof Validator) {
				continue;
			}

			$list = $list->withValidator($validator);
		}

		return $list;
	}

	public static function fromValidators(Validator ...$validators): self
	{
		return new self(...$validators);
	}

	public function withValidator(Validator $validator): self
	{
		return new self($validator, ...$this->validators);
	}

	/**
	 * @return Validator[]
	 */
	private function &getList(): array
	{
		return $this->validators;
	}
}
