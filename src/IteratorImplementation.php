<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\Password;

use function current;
use function key;
use function next;
use function reset;

trait IteratorImplementation
{
	abstract private function &getList(): array;

	public function key(): mixed
	{
		return key($this->getList());
	}

	public function valid(): bool
	{
		return $this->current() !== false;
	}

	public function current(): mixed
	{
		return current($this->getList());
	}

	public function next(): void
	{
		next($this->getList());
	}

	public function rewind(): void
	{
		reset($this->getList());
	}
}
