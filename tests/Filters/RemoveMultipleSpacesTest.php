<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Filter;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Filters\RemoveMultipleSpaces;
use StellaMaris\Password\Password;

class RemoveMultipleSpacesTest extends TestCase
{
	/**
	 * @testdox Removing double or muliple whitespaces from "$password"
	 * @dataProvider dataProvider
	 * @covers \StellaMaris\Password\Filters\RemoveMultipleSpaces::filter
	 */
	public function testFilterWorks(string $password, string $expected): void
	{
		$filter = new RemoveMultipleSpaces();

		$newPassword = $filter->filter(Password::fromString($password));

		self::assertSAme($expected, $newPassword->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave());
	}

	public function dataProvider(): array
	{
		return [
			['P    a', 'P a'],
			['Password', 'Password'],
			['P  a  s  s  w   o    r     d', 'P a s s w o r d'],
		];
	}
}
