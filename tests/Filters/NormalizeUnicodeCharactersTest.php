<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Filter;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Filters\NormalizeUnicodeCharacters;
use StellaMaris\Password\Password;
use function hex2bin;

class NormalizeUnicodeCharactersTest extends TestCase
{
	/**
	 * @testdox The string "$password" is normalized to NFKC form
	 * @dataProvider dataProvider
	 * @covers \StellaMaris\Password\Filters\NormalizeUnicodeCharacters::filter
	 */
	public function testFilterWOrksAsExpected(string $password, string $expected): void
	{
		$filter = new NormalizeUnicodeCharacters();

		$newPassword = $filter->filter(Password::fromString($password));

		self::assertSame($newPassword->yesIDoNeedThePasswordInCleartextAndIKnowOfTheImplicationsThatMightHave(), $expected);
	}

	public function dataProvider(): array
	{
		return [
			['test', 'test'],
			[hex2bin('61cc886fcc8875cc88c39f'), hex2bin('c3a4c3b6c3bcc39f')],
			[hex2bin('61cc886fcc8875cc88c39f'), 'äöüß'],
			['äöüß', 'äöüß'],
		];
	}
}
