<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Filter;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Filters\FilterList;
use StellaMaris\Password\Filters\FilterListFactory;

class FilterListFactoryTest extends TestCase
{
	/**
	 * @testdox The FilterListFactory creates a FilterList
	 * @covers \StellaMaris\Password\Filters\FilterListFactory::create
	 */
	public function testFilterListFactoryReturnsAnEMptyFilterlist(): void
	{
		self::assertInstanceof(FilterList::class, FilterListFactory::create());
	}
}
