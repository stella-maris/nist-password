<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Filter;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Filters\Filter;
use StellaMaris\Password\Filters\FilterList;

class FilterListTest extends TestCase
{
	/**
	 * @testdox The FilterList adds and iterates over Filters
	 * @covers \StellaMaris\Password\Filters\FilterList::fromArray
	 * @covers \StellaMaris\Password\Filters\FilterList::__construct
	 * @covers \StellaMaris\Password\Filters\FilterList::withFilter
	 * @covers \StellaMaris\Password\Filters\FilterList::getList
	 * @covers \StellaMaris\Password\IteratorImplementation::current
	 * @covers \StellaMaris\Password\IteratorImplementation::key
	 * @covers \StellaMaris\Password\IteratorImplementation::next
	 * @covers \StellaMaris\Password\IteratorImplementation::valid
	 * @covers \StellaMaris\Password\IteratorImplementation::rewind
	 */
	public function testAddingAndRetrievingFilters(): void
	{
		$list = FilterList::fromArray([]);

		$i = 0;
		foreach ($list as $item) {
			$i++;
		}

		$this->assertSame(0, $i);

		$filter = $this->getMockBuilder(Filter::class)->getMock();
		$list1 = $list->withFilter($filter);

		$this->assertNotSame($list, $list1);

		$i = 0;
		$item = null;
		foreach ($list1 as $item) {
			$i++;
		}

		self::assertSame(1, $i);
		self::assertSame($item, $filter);
	}

	/**
	 * @testdox The FilterList can be instantiated with an array of filters
	 * @covers \StellaMaris\Password\Filters\FilterList::fromArray
	 * @covers \StellaMaris\Password\Filters\FilterList::__construct
	 * @covers \StellaMaris\Password\Filters\FilterList::getList
	 * @covers \StellaMaris\Password\IteratorImplementation::current
	 * @covers \StellaMaris\Password\IteratorImplementation::key
	 * @covers \StellaMaris\Password\IteratorImplementation::next
	 * @covers \StellaMaris\Password\IteratorImplementation::valid
	 * @covers \StellaMaris\Password\IteratorImplementation::rewind
	 */
	public function testAddingFiltersViaArray(): void
	{
		$filter1 = $this->getMockBuilder(Filter::class)->getMock();
		$filter2 = $this->getMockBuilder(Filter::class)->getMock();

		$list = FilterList::fromArray([$filter1, 'test', $filter2]);

		$i = 0;
		foreach ($list as $item) {
			$i++;
		}

		$this->assertSame(2, $i);

		self::assertSame($item, $filter1);
	}

	/**
	 * @testdox An empty FilterList *is* empty
	 * @covers \StellaMaris\Password\Filters\FilterList::empty
	 * @covers \StellaMaris\Password\Filters\FilterList::__construct
	 * @covers \StellaMaris\Password\Filters\FilterList::getList
	 * @covers \StellaMaris\Password\IteratorImplementation::current
	 * @covers \StellaMaris\Password\IteratorImplementation::key
	 * @covers \StellaMaris\Password\IteratorImplementation::next
	 * @covers \StellaMaris\Password\IteratorImplementation::valid
	 * @covers \StellaMaris\Password\IteratorImplementation::rewind
	 */
	public function testCreatingEmptyList(): void
	{
		$list = FilterList::empty();

		$i = 0;
		foreach ($list as $item) {
			$i++;
		}

		$this->assertSame(0, $i);
	}

	/**
	 * @testdox An empty FilterList *is* empty
	 * @covers \StellaMaris\Password\Filters\FilterList::fromFilters
	 * @covers \StellaMaris\Password\Filters\FilterList::__construct
	 * @covers \StellaMaris\Password\Filters\FilterList::getList
	 * @covers \StellaMaris\Password\IteratorImplementation::current
	 * @covers \StellaMaris\Password\IteratorImplementation::key
	 * @covers \StellaMaris\Password\IteratorImplementation::next
	 * @covers \StellaMaris\Password\IteratorImplementation::valid
	 * @covers \StellaMaris\Password\IteratorImplementation::rewind
	 */
	public function testCreatingListFromFilters(): void
	{
		$filter1 = $this->getMockBuilder(Filter::class)->getMock();
		$filter2 = $this->getMockBuilder(Filter::class)->getMock();

		$list = FilterList::fromFilters($filter1, $filter2);

		$i = 0;
		foreach ($list as $item) {
			$i++;
		}

		$this->assertSame(2, $i);

		self::assertSame($item, $filter2);
	}
}
