<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Validate\Validators;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validators\NoRepetitiveCharacters;

class NoRepetitiveCharactersTest extends TestCase
{
	/**
	 * @testdox password "$password" is recognized as having repeating characters
	 * @covers \StellaMaris\Password\Validate\Validators\NoRepetitiveCharacters::validate
	 * @covers \StellaMaris\Password\Validate\Validators\NoRepetitiveCharacters::getProblem
	 * @dataProvider provideREpetitivePasswords
	 */
	public function testPasswordWithRepetitiveCHaractersIsInvalid(string $password): void
	{
		$validator = new NoRepetitiveCharacters();

		self::assertInstanceof(Problem::class, $validator->getProblem());

		self::assertFalse($validator->validate(Password::fromString($password)));
	}

	public function provideREpetitivePasswords(): array
	{
		return [
			['aaa'],
			['Paaaswwwordddd'],
			['papapapa'],
		];
	}
}
