<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Validate\Validators;

use Dragonbe\Hibp\Hibp;
use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach;

class PasswordHasNotBeenExposedInBreachTest extends TestCase
{
	/**
	 * @testdox Pwned Password results in Problem
	 * @covers \StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach::validate
	 * @covers \StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach::getProblem
	 */
	public function testPwnedPasswordResultsInProblem(): void
	{
		$hibp = $this->getMockBuilder(Hibp::class)->disableOriginalConstructor()->getMock();
		$validator = new PasswordHasNotBeenExposedInBreach($hibp);
		self::assertInstanceof(Problem::class, $validator->getProblem());

		$hibp->method('isPwnedPassword')->with('foo', false)->willReturn(true);
		self::assertFalse($validator->validate(Password::fromString('foo')));
	}

	/**
	 * @testdox un-Pwned Password results in valid password
	 * @covers \StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach::validate
	 * @covers \StellaMaris\Password\Validate\Validators\PasswordHasNotBeenExposedInBreach::getProblem
	 */
	public function testUnPwnedPasswordResultsInValidPassword(): void
	{
		$hibp = $this->getMockBuilder(Hibp::class)->disableOriginalConstructor()->getMock();
		$validator = new PasswordHasNotBeenExposedInBreach($hibp);
		self::assertInstanceof(Problem::class, $validator->getProblem());

		$hibp->method('isPwnedPassword')->with('foo', false)->willReturn(false);
		self::assertTrue($validator->validate(Password::fromString('foo')));
	}
}
