<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Validate\Validators;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validators\NoSequentialCharacters;

class NoSequentialCharactersTest extends TestCase
{
	/**
	 * @testdox password "$password" is recognized as having sequential characters
	 * @covers \StellaMaris\Password\Validate\Validators\NoSequentialCharacters::validate
	 * @covers \StellaMaris\Password\Validate\Validators\NoSequentialCharacters::getProblem
	 * @dataProvider provideSequentialPasswords
	 */
	public function testPasswordWithSequentialCharactersIsInvalid(string $password): void
	{
		$validator = new NoSequentialCharacters();

		self::assertInstanceof(Problem::class, $validator->getProblem());

		self::assertFalse($validator->validate(Password::fromString($password)));
	}

	/**
	 * @testdox password "aabbccdd" is recognized as not having sequential characters
	 * @covers \StellaMaris\Password\Validate\Validators\NoSequentialCharacters::validate
	 */
	public function testPasswordWithSlightSequentialCharactersIsValid(): void
	{
		$validator = new NoSequentialCharacters();

		self::assertTrue($validator->validate(Password::fromString('aabbccdd')));
	}

	public function provideSequentialPasswords(): array
	{
		return [
			['abcdefgh'],
			['12345'],
			['ABCDE'],
			['αβγδ'],
			['εζηθ'],
			['ΑβΓδΕζ'],
			['ΑβΓδ'],
			['δΓβδΓβΑ'],
		];
	}
}
