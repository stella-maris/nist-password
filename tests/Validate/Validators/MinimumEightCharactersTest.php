<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licenses under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest\Validate\Validators;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Problem;
use StellaMaris\Password\Validate\Validators\MinimumEightCharacters;

class MinimumEightCharactersTest extends TestCase
{
	/**
	 * @testdox A password with less than 8 characters results in a problem
	 * @covers \StellaMaris\Password\Validate\Validators\MinimumEightCharacters::validate
	 * @covers \StellaMaris\Password\Validate\Validators\MinimumEightCharacters::getProblem
	 */
	public function testPasswordWithLessTHanEightCHaractersIsInvalid(): void
	{
		$validator = new MinimumEightCharacters();

		self::assertInstanceof(Problem::class, $validator->getProblem());

		foreach (['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa'] as $i) {
			self::assertFalse($validator->validate(Password::fromString($i)));
		}
	}

	/**
	 * @testdox A password with more or equals to 8 characters is considered valid
	 * @covers \StellaMaris\Password\Validate\Validators\MinimumEightCharacters::validate
	 */
	public function testPasswordHasAtLeastEightCharacters(): void
	{
		$validator = new MinimumEightCharacters();

		self::assertTrue($validator->validate(Password::fromString('aaaaaaaa')));
		self::assertTrue($validator->validate(Password::fromString('aaaaaaaaaa')));
	}
}
