<?php

declare(strict_types=1);

/**
 * Copyright Stella-Maris Solutions <andreas@stella-maris.solutions>
 *
 * Licensed under the GPL-3-Clause License. For details see the included file LICENSE.md
 */

namespace StellaMaris\PasswordTest;

use PHPUnit\Framework\TestCase;
use StellaMaris\Password\Filters\FilterListFactory;
use StellaMaris\Password\Password;
use StellaMaris\Password\Validate\Validate;
use StellaMaris\Password\Validate\ValidatorListFactory;

class GeneralTest extends TestCase
{
	/**
	 * @testdox Password $password fails
	 * @dataProvider failingPasswords
	 * @covers \StellaMaris\Password\Password
	 * @covers \StellaMaris\Password\Validate\Validate
	 */
	public function testValidation(string $password): void
	{
		$password = Password::fromString($password);

		$validator = new Validate(
			ValidatorListFactory::create(),
			FilterListFactory::create(),
		);
		$feedback = $validator->validate($password);

		self::assertFalse($feedback->isValid());
	}

	public function failingPasswords(): array
	{
		return [
			['test'],
			['password'],
			['Password1234'],
			['Pass     wr'],
		];
	}
}
